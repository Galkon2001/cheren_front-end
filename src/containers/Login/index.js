import React from "react";
import Login from "./Login";
export const Signin = props => <Login {...props} isSignin />;
export const Signup = props => <Login {...props} isSignup />;
export const Forgot = props => <Login {...props} isForgot />;
export const NewPass = props => <Login {...props} isNewPass />;
