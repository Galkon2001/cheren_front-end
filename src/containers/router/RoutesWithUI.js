import React from "react";
import { connect } from "react-redux";
import { Switch, Redirect } from "react-router-dom";
import styled from "styled-components/macro";
// import Admin from 'containers/Admin';
// import { Header } from 'components';
// import { ACCESS_LEVEL } from 'constant';
// import { isAccessLevel } from 'utils';
// import AdminMap from '../AdminMap/AdminMap';
// import CompanyMap from '../CompanyMap/CompanyMap';
// import LocationMap from '../LocationMap/LocationMap';
// import ProtectedRoute from './ProtectedRoute';
// import Sidebar from '../Sidebar/Sidebar';
// import OwnerDashbaord from './OwnerDashbaord';
// import ResidentRoutes from './ResidentRoutes';
// import LocationInfo from 'containers/LocationInfo';
// import InspectionTable from 'containers/InspectionTable/InspectionTable';
// import InspectionAccept from 'containers/InspectionTable/InspectionAccept';
// import InspectionKesprySearch from 'containers/InspectionTable/InspectionKesprySearch';
// import ActionRequestTable from 'containers/ActionRequestTable/ActionRequestTable';

// const { RESIDENT, OWNER, ASSOCIATE_ADMIN, SUPER_ADMIN } = ACCESS_LEVEL;

const RoutesWithUI = ({ startPage }) => {
  return (
    <Container>
      informations
      {/* <Header />
      <Content>
        <Sidebar />
        <InnerContent>
          <ContentWrapper>
            <Switch>
              <ProtectedRoute
                access={[SUPER_ADMIN]}
                path="/"
                exact
                component={AdminMap}
              />
              <ProtectedRoute
                access={[OWNER, ASSOCIATE_ADMIN]}
                exact
                path="/map"
                component={CompanyMap}
              />
              <ProtectedRoute
                access={[RESIDENT, OWNER, ASSOCIATE_ADMIN]}
                exact
                path="/location/:locationId"
                component={LocationMap}
              />
              <ProtectedRoute
                access={[RESIDENT, OWNER, ASSOCIATE_ADMIN, SUPER_ADMIN]}
                path="/location/:locationId/info"
                component={LocationInfo}
              />
              <ProtectedRoute access={[SUPER_ADMIN]} path="/admin" component={Admin} />
              <ProtectedRoute
                access={[SUPER_ADMIN]}
                exact
                path="/inspection-requests"
                component={InspectionTable}
              />
              <ProtectedRoute
                access={[SUPER_ADMIN]}
                exact
                path="/inspection-requests/:inspectionId/accept"
                component={InspectionAccept}
              />
              <ProtectedRoute
                access={[SUPER_ADMIN]}
                exact
                path="/inspection-requests/kespry-search"
                component={InspectionKesprySearch}
              />
              <ProtectedRoute
                access={[SUPER_ADMIN]}
                exact
                path="/action-request"
                component={ActionRequestTable}
              />
              {isAccessLevel(RESIDENT) && (
                <ProtectedRoute access={[RESIDENT]} component={ResidentRoutes} />
              )}
              <ProtectedRoute
                access={[OWNER, ASSOCIATE_ADMIN]}
                component={OwnerDashbaord}
              />
              {startPage !== '/' && <Redirect from="/" exact to={startPage} />}
            </Switch>
          </ContentWrapper>
          <Footer>© 2019 Superstorm Restoration. All rights reserved.</Footer>
        </InnerContent>
      </Content> */}
    </Container>
  );
};

const mapState = ({ auth }) => ({
  startPage: auth.startPage
});

export default connect(mapState)(RoutesWithUI);

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

const Content = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  width: 100%;
`;

const InnerContent = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  width: calc(100% - 280px);
`;

const ContentWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  padding: 35px 55px;
  overflow-y: auto;
`;

const Footer = styled.footer`
  flex-shrink: 0;
  border-top: 1px solid #e3e8ee;
  color: #99afc6;
  padding-top: 30px;
  font-weight: 700;
  font-size: 12px;
  margin: 20px 55px 35px;
`;
