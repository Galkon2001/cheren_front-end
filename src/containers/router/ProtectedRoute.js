import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { isAccessLevel } from 'utils';
import Preloader from '../Preloader/Preloader';

class ProtectedRoute extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isUserRequesting: true,
    };
  }

  async componentDidMount() {
    const { auth, authToken, loadInfo } = this.props;
    if (authToken && !auth._id) {
      try {
        await loadInfo(authToken);
      } catch (err) {
        console.error(err);
      }
    }
    this.setState({ isUserRequesting: false });
  }
  render() {
    const { component: Component, authToken, auth, access, exact, path } = this.props;
    const { isUserRequesting } = this.state;
    const hasAccess = !access || (auth._id && access && isAccessLevel(...access));

    if (!authToken) return <Redirect to={{ pathname: '/login' }} />;

    return isUserRequesting ? (
      <Preloader />
    ) : hasAccess ? (
      <Route exact={exact} path={path} render={props => <Component {...props} />} />
    ) : (
      <Redirect to={auth.startPage} />
    );
  }
}

const mapState = ({ auth, authToken }) => ({ auth, authToken });
const mapDispatch = ({ auth }) => ({
  loadInfo: auth.loadInfo,
});

export default connect(
  mapState,
  mapDispatch
)(ProtectedRoute);
