import React from "react";
import { Route, Switch } from "react-router-dom";
import RoutesWithUI from "./RoutesWithUI";
import { Signin, Signup, Forgot, NewPass } from "../Login";

const Router = () => {
  return (
    <div className="router">
      <Switch>
        <Route path="/signin" component={Signin} />
        <Route path="/signup" component={Signup} />
        <Route path="/forgotpass" component={Forgot} />
        <Route path="/newpass" component={NewPass} />
        <Route component={RoutesWithUI} />
      </Switch>
    </div>
  );
};

export default Router;
