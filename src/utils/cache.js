const cache = {};
export default cache;

const MAX_CACHE_SIZE = 104857600; // 100MB

const getCacheSize = cache =>
  Object.values(cache).reduce(
    (acc, item) => acc + (item.length || item.base64.length) * 2,
    0
  );
export const deleteFromCache = key => {
  delete cache[key];
};

export const saveToCache = (key, value) => {
  cache[key] = value;

  if (getCacheSize(cache) > MAX_CACHE_SIZE) {
    let i = 0;
    while (getCacheSize(cache) > MAX_CACHE_SIZE) {
      const key = Object.keys(cache)[i];
      delete cache[key];
      i = i + 1;
    }
  }

  // console.log(
  //   'Current cache size',
  //   +(getCacheSize(cache) / 1024 ** 2).toFixed(2),
  //   ' Limit:',
  //   MAX_CACHE_SIZE / 1024 ** 2
  // );
};
