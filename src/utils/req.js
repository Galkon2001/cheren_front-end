import axios from "axios";
import Сonfig from "../config/config.js";
import store from "../store";
const { API, AUTH_HEADER_NAME } = Сonfig;

axios.interceptors.request.use(
  reqConf => {
    const token = store.getState().authToken;
    if (token) {
      reqConf.headers[AUTH_HEADER_NAME] = token;
    }
    return reqConf;
  },
  error => Promise.reject(error)
);

//console.log(API);
export default {
  post(route, config) {
    return axios.post(`${API}/${route}`, config);
  }
};
