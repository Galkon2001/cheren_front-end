import dayjs from "dayjs";
import en from "dayjs/locale/en";
import LocalizedFormat from "dayjs/plugin/localizedFormat";
import customParseFormat from "dayjs/plugin/customParseFormat";
import dayjsPluginUTC from "dayjs-plugin-utc";

dayjs.extend(LocalizedFormat);
dayjs.extend(customParseFormat);
dayjs.extend(dayjsPluginUTC);

const locale = {
  ...en,
  weekdaysMin: "S_M_T_W_T_F_S".split("_"),
  formats: {
    LT: "HH:mm",
    LTS: "HH:mm:ss",
    L: "MM.DD.YYYY",
    l: "DD MMM",
    LL: "D MMMM YYYY",
    LLL: "D MMMM YYYY HH:mm",
    LLLL: "dddd, D MMMM YYYY HH:mm"
  }
};

dayjs.locale(locale, null, true);

export const parseDayjs = date => {
  let format;
  if (dayjs(date, "MM.DD.YYYY").isValid()) {
    format = "MM.DD.YYYY";
  }
  return dayjs(date, format);
};

export const dateFormat = "YYYY-MM-DD";

export default dayjs;
