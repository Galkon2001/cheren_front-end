import React from "react";
// import styled from "styled-components";
import FontsStyles from "./static/styles/fonts";
import GlobalStyles from "./static/styles";
import Router from "./containers/router/Router";

function App() {
  return (
    <div className="App">
      <Router />
      <GlobalStyles />
      <FontsStyles />
    </div>
  );
}

export default App;
