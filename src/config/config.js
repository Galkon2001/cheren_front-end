const isProd = process.env.NODE_ENV === "production";
const { protocol, hostname } = window.location;

export default {
  API: `${protocol}//${hostname}${isProd ? "" : ":3007"}`,
  AUTH_HEADER_NAME: "authorization",
  AUTH_TOKEN_NAME: "token",
  SERVER_STATIC_PATH: "/public"
};
