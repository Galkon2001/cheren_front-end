import createRematchPersist from "@rematch/persist";
import { init } from "@rematch/core";

import * as models from "./models";

const persistPlugin = createRematchPersist({
  whitelist: ["authToken"],
  version: 1
});

const store = init({
  models,
  plugins: [persistPlugin]
});

export default store;
